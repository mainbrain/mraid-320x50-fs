# MainBrain MRAID Template

MainBrain MRAID 320x50 + FS Expanded

## Template usage

Property `mbAd.ready` will return a `Promise` that will be resolved once template setup is done, and can be used as in the example bellow.

```javascript
mbAd.ready.then(() => {
  // Called when creative is ready.
});
```

### Orientation

Calling function `mbAd.getOrientationOptions()` will return an object with specified values:

```json
{
  "allowOrientationChange" : true|false,
  "forceOrientation" : "portrait|landscape|none"
}
```

Function `mbAd.setOrientationOptions(allow, force)` can be used to set orientation options at any time after template is ready, and used multiple times during creative exposure.

Usage:
```javascript
mbAd.setOrientationOptions(true|false|null, mbAd.Orientation.*|null);
```

Example: Will force device portrait mode exclusively from then on, until changed or creative is closed.
```javascript
mbAd.setOrientationOptions(false, mbAd.Orientation.portrait);
```

## MRAID Test Apps

Apple Store

https://apps.apple.com/pt/app/creative-preview/id1031988940?l=en

Google Play

https://play.google.com/store/apps/details?id=com.google.android.apps.audition